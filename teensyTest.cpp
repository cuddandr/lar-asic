#include "teensySerial.h"

int main()
{
    teensySerial teensy;
    
    teensy.initSerial("/dev/ttyACM0");
    teensy.setDebug(1);
    teensy.setOutput(0);

    //teensy.sendArray("config.txt");
    
        
    teensy.edit("A", "1", "16");
    teensy.edit("F", "48");
    teensy.global("C", "1", "3");
    teensy.global("D", "4");
    teensy.global("AA", "15", "30");
    
    teensy.print();
    
    teensy.save();
    teensy.reset();
    teensy.print();
    
    teensy.load();
    teensy.print();

    teensy.setBit("SBF", "49", "96");
    teensy.setBit("SBF", "17");
    teensy.setBit("SLK", "1", "3", "global");
    teensy.setBit("SLK", "5", "5", "global");
    teensy.clearBit("SBF", "48");
    teensy.clearBit("SLK", "2", "2", "global");
    
    teensy.shift("0");
    
    //teensy.debugConsole();
    teensy.closeSerial();
    return 0;
}
