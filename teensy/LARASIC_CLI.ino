//Program: Command Line Interface for Programming LArASIC's
//       : Main Hub Software
//Author : Andrew Cudd
//Email  : cuddandr@msu.edu
//Date   : 6/25/14
//Version: 1.00
///////////////////////////////////////////////////////////

//Include libraries
#include <EEPROM.h>
#include <CLI.h>

#include <avr/pgmspace.h>
//Store the help output in program memory instead of SRAM
prog_char help_line1[] PROGMEM = "-------HELP-------";
prog_char help_line2[] PROGMEM = "print [base] : prints the entire config array with the optional base, defaults to hex";
prog_char help_line3[] PROGMEM = "edit [value] [start] [end] : stores the value (in hex) in the config array between the start and end";
prog_char help_line4[] PROGMEM = "global [value] [start] [end] : identical to edit, except it stores the values in the global register entries";
prog_char help_line5[] PROGMEM = "reset : resets the config array to all zeros";
prog_char help_line6[] PROGMEM = "save : stores the config array in the EEPROM";
prog_char help_line7[] PROGMEM = "load : loads the config array from the EEPROM";
prog_char help_line8[] PROGMEM = "set [bit] [start] [end] : sets the bit in the config array between start and end";
prog_char help_line9[] PROGMEM = "clear [bit] [start] [end] : clears the bit in the config array between start and end";
prog_char help_line10[] PROGMEM = "shift [mode] : shifts bytes out based on the mode";

//Table data structure for the help function
PROGMEM const char* help_table[] = {help_line1, help_line2, help_line3, help_line4, help_line5, help_line6, help_line7, help_line8,
                                    help_line9, help_line10};

//Name of Channel Register bits
const char* channel_reg[8] = {"STS", "SNC", "SG0", "SG1", "ST0", "ST1", "SDC", "SBF"};

//Name of Global Register bits
const char* global_reg[4] = {"S16", "STB", "STB1", "SLK"};

//LArASIC Config Array
byte config_array[num_boards][num_channel + num_global];

void setup()
{
    //Set up serial communications
    CLI_init(19200);
    //Set up pins for shifting
    shift_init();
}

//Run forever and ever...
void loop()
{
    //See if there is anything to read
    if (readline(Serial.read(), buf) > 0)
    {
        //Edit a range of config registers
        if(strcmp(buf[0], "edit") == 0)
            edit_channel_reg(buf);

        //Edit a range of global registers
        else if(strcmp(buf[0], "global") == 0)
            edit_global_reg(buf);

        //Print Command
        else if(strcmp(buf[0], "print") == 0)
        {
            //If no parameter is given for the base, default to HEX (base 16)
            if(buf[1] == '\0')
                buf[1] = "16";
            //Turn string into a number
            int base = strtol(buf[1], NULL, 10);
            //Print the array with the specified base
            print_array(base);
        }

        //Reset Command
        else if(strcmp(buf[0], "reset") == 0)
            reset_array();

        //Shift the array to the LArASIC's
        else if(strcmp(buf[0], "shift") == 0)
            shift_array(buf);

        //Save config array to interal EEPROM
        else if(strcmp(buf[0], "save") == 0)
            save_array();

        //Load config array from interal EEPROM
        else if(strcmp(buf[0], "load") == 0)
            load_array();

        //Set a single bit of a given channel
        else if(strcmp(buf[0], "set") == 0)
            set_bit(buf);

        //Clear a single bit of a given channel
        else if(strcmp(buf[0], "clear") == 0)
            clear_bit(buf);

        //Send a test pulse to the asic
        else if(strcmp(buf[0], "test") == 0)
            test_signal(buf);

        //Print help message
        else if(strcmp(buf[0], "help") == 0)
            help(help_table);

        //Catch any other command
        else
        {
            Serial.println();
            Serial.print("Command not recognized. Type 'help' for list of commands.");
        }

        CLI_refresh();
    }
}
