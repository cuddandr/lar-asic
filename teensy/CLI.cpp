#include <EEPROM.h>
#include "CLI.h"

void CLI_init(int baud_rate)
{
    //Start serial connection
    Serial.begin(baud_rate);
    //Command line prompt
    Serial.print("LArIAT@Fermi > ");
}

void CLI_refresh()
{
    //Print blank line then prompt
    Serial.println();
    Serial.print("LArIAT@Fermi > ");
}

void shift_init()
{
    //Set up pins for shifting out data
    pinMode(test_pin, OUTPUT);
    pinMode(reset_pin, OUTPUT);
    pinMode(chip_select_pin, OUTPUT);
    pinMode(clock_pin, OUTPUT);
    pinMode(data_pin, OUTPUT);
    pinMode(LED_pin, OUTPUT);
}

char readline(char readch, char* buffer[])
{
    //Variable Declarations
    static char pos = 0;        //Position in the buffer array
    static char msg[max_size];  //Array for raw serial input
    byte arg = 0;               //Number of arguments given
    byte i = 0;                 //Index variable
    byte rpos;                  //Position of carriage return

    //If there is a character...
    if (readch > 0)
    {
        //... do things to it.
        switch(readch)
        {

            //Process backspace characters
            case '\b':
                //Adjust position in serial array
                pos--;
                //Display the deletion of the last character
                Serial.print('\b');
                Serial.print(' ');
                Serial.print('\b');
                break;

            //Process input on carriage return (e.g. enter key)
            case '\r':
                rpos = pos;  //Set CR position
                pos = 0;     //Reset position index

                //Separate command and arguments from each other
                buffer[i] = strtok(msg, " "); //Using the space character as the separator
                while(buffer[i] != NULL)
                {
                    /*
                    Serial.println();
                    for(int j = 0; j < i; j++)
                    {
                        Serial.print("Buffer ");
                        Serial.print(j);
                        Serial.print(": ");
                        Serial.println(buffer[j]);
                    }
                    */
                    buffer[++i] = strtok(NULL, " ");
                }

                /*
                Serial.println();
                for(int j = 0; j < i; j++)
                {
                    Serial.print("Buffer ");
                    Serial.print(j);
                    Serial.print(": ");
                    Serial.println(buffer[j]);
                }
                */

                //Save number of arguments given
                arg = i;
                return rpos;

            //Process any other character
            default:
                //If there is still space, put the character in the array
                if(pos < max_size - 1)
                {
                    msg[pos++] = readch;  //Store character and advance position
                    msg[pos] = '\0';      //Keep array properly 'Null-terminated'
                }
                Serial.print(readch);     //Print character to screen
                break;
        }
    }
    //No end of line has been found, so return -1.
    return -1;
}

void edit_channel_reg(char* buf[])
{
    //Local Variable Declarations
    int start_index, end_index, raw_channel;
    byte card, adj_channel;

    //Get the start_index and end_index numbers
    start_index = strtol(buf[2], NULL, 10) - 1;
    //If an end_index number was entered
    if(buf[3] != NULL)
        //Store the number
        end_index = strtol(buf[3], NULL, 10) - 1;
    else
        //If no end_index was given, then set the end_index equal to the start_index.
        //This then only edits a single byte.
        end_index = start_index;

    //Check the range of the channels given
    if((start_index > -1) && (end_index < 480))
    {
        //If range was valid, edit all of the given channels
        for(raw_channel = start_index; raw_channel <= end_index; raw_channel++)
        {
            //Map the channel number to an entry in the array
            card = ceil(raw_channel / num_channel);
            adj_channel = raw_channel % num_channel;
            //Set the corresponding entry to the value given in hex
            config_array[card][adj_channel] = static_cast<byte>(strtol(buf[1], NULL, 16));
        }
    }
    //Error message
    else
    {
        Serial.println();
        Serial.print("Invalid Channel Range");
    }
}

void edit_global_reg(char* buf[])
{
    //Local Variable Declarations
    int start_index, end_index, raw_global;
    byte card, adj_global;

    //Get the start_index and end_index numbers
    start_index = strtol(buf[2], NULL, 10) - 1;
    //If an end_index number was entered
    if(buf[3] != NULL)
        //Store the number
        end_index = strtol(buf[3], NULL, 10) - 1;
    else
        //If no end_index was given, then set the end_index equal to the start_index.
        //This then only edits a single byte.
        end_index = start_index;

    //Check the range of the channels given
    if((start_index > -1) && (end_index < 30))
    {
        //If range was valid, edit all of the given channels
        for(raw_global = start_index; raw_global <= end_index; raw_global++)
        {
            //Map the channel number to an entry in the array
            card = ceil(raw_global / num_global);
            adj_global = raw_global % num_global + 48;  //The +48 is to place the entry at the end of the row
            //Set the corresponding entry to the value given in hex
            config_array[card][adj_global] = static_cast<byte>(strtol(buf[1], NULL, 16));
        }
    }
    //Error message
    else
    {
        Serial.println();
        Serial.print("Invalid Global Range");
    }
}

void print_array(int base)
{
    //Print new line
    Serial.println();
    //Print each value of the config_array
    for(byte i=0; i<10; i++)
    {
        for(byte j=0; j<51; j++)
        {
            //Print each element in the base specified
            Serial.print(config_array[i][j], base);
            Serial.print(" ");
        }
    //Another new line for readability
    Serial.println();
    }
}

void reset_array()
{
    //Set each element of config array to zero
    for(byte i=0; i<10; i++)
    {
        for(byte j=0; j<51; j++)
            config_array[i][j] = 0;
    }
}

//Shift the array to the LArASIC's
void shift_array(char* buf[])
{
    //Shift the entire array, start to finish
    if(strcmp(buf[1], "all") == 0)
    {
        //Set clock polarity; FALLING is default
        byte clock_pol;
        if(strcmp(buf[2], "rising") == 0)
            clock_pol = RISING;
        else
            clock_pol = FALLING;

        //Default value for optional delay
        if(buf[3] == '\0')
            buf[3] = "0";

        int delay = strtol(buf[2], NULL, 10);

        //Variables to set loop indicies
        int board_start = num_boards - 1;
        int channel_start = num_channel + num_global - 1;
        byte channel_out = 0;

        //Outer loop through each cards (rows)
        for(int i = board_start; i >= 0; i--)
        {
            //Inner loop through each ASIC chip (columns)
            for(int j = channel_start; j >= 0; j--)
            {
                //This block of if / else if's is to turn the original array mapping
                //into the mapping the LArASIC's are expecting. More is explained in
                //the README.

                if((34 < j) && (j <= 50))
                    channel_out = j-3;
                else if((17 < j) && (j <= 33))
                    channel_out = j-2;
                else if((0 < j) && (j <= 16))
                    channel_out = j-1;
                else if(j == 34)
                    channel_out = 50;
                else if(j == 17)
                    channel_out = 49;
                else if(j == 0)
                    channel_out = 48;

                //Set the Chip Select High
                digitalWrite(chip_select_pin, HIGH);
                //Shift the data out, given the parameters
                shift_out(data_pin, clock_pin, config_array[i][channel_out], LSBFIRST, clock_pol, delay);
                //Set the Chip Select Low, latching the data into the LArASIC's
                digitalWrite(chip_select_pin, LOW);
            }
        }
        //Output message
        Serial.println();
        Serial.print("Done.");
    }

    //Shift the test byte entered into the buffer
    if(strcmp(buf[1], "test") == 0)
    {
        byte clock_pol;
        if(strcmp(buf[3], "rising") == 0)
            clock_pol = RISING;
        else
            clock_pol = FALLING;

        if(buf[4] == '\0')
            buf[4] = "0";

        int delay = strtol(buf[3], NULL, 10);

        digitalWrite(chip_select_pin, HIGH);
        shift_out(data_pin, clock_pin, strtol(buf[2], NULL, 16), LSBFIRST, clock_pol, delay);
        digitalWrite(chip_select_pin, LOW);
    }
}

//The actual routine to shift data out on the data_pin
void shift_out(byte data_pin, byte clock_pin, byte val, byte bit_order, byte clock_pol, int delay)
{
    //Variable for the clock_pin state
    int clock_state;

    //If clock polarity is set to RISING, then a LOW-to-HIGH clock pulse is
    //supplied to the device. FALLING produces a HIGH-to-LOW clock and is the
    //default mode.
    if(clock_pol == RISING)
        clock_state = LOW;
    else
        clock_state = HIGH;

    //Set the Clock and Data pins to default
    digitalWrite(clock_pin, clock_state);
    digitalWrite(data_pin, LOW);

    //Loop through the byte
    for (byte i = 0; i < 8; i++)
    {
        //Send the bits out either LSB or MSB first
        if (bit_order == LSBFIRST)
            digitalWrite(data_pin, !!(val & (1 << i)));
        else
            digitalWrite(data_pin, !!(val & (1 << (7 - i))));

        //Set Clock State; LED High
        digitalWrite(clock_pin, clock_state);
        //digitalWrite(LED_pin, HIGH);
        //Optional delay between clock pulses
        delayMicroseconds(delay);
        //Invert Clock; LED Low
        digitalWrite(clock_pin, !clock_state);
        //digitalWrite(LED_pin, LOW);
        //Restore Clock to inital value
        //clock_state != clock_state;
        delayMicroseconds(delay);
    }
}

void save_array()
{
    //Store each element in the EEPROM
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 51; j++)
            EEPROM.write(51*i + j, config_array[i][j]);
    }
}

void load_array()
{
    //Load each element from the EEPROM
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 51; j++)
            config_array[i][j] = EEPROM.read(51*i + j);
    }
}

void set_bit(char* buf[])
{
    //Local Variable Declarations
    int start_index, end_index, raw_channel, raw_global;
    byte card, adj_channel, adj_global;

    //Get the start_index and end_index numbers
    start_index = strtol(buf[2], NULL, 10) - 1;

    //Work-around to not have a seperate set_global bit command, and still
    //maintain the optional end argument
    if(strcmp(buf[3], "global") == 0)
    {
        buf[3] = NULL;
        buf[4] = "global";
    }

    //If an end_index number was entered
    if(buf[3] != NULL)
        //Store the number
        end_index = strtol(buf[3], NULL, 10) - 1;
    else
        //If no end_index was given, then set the end_index equal to the start_index.
        //This then only edits a single byte.
        end_index = start_index;

    //Check if global or channel bits
    if(strcmp(buf[4], "global") == 0)
    {
        if((start_index > -1) && (end_index < 30))
        {
            //If range was valid, edit all of the given channels
            for(raw_global = start_index; raw_global <= end_index; raw_global++)
            {
                //Map the channel number to an entry in the array
                card = ceil(raw_global / num_global);
                adj_global = raw_global % num_global + 48;  //The +48 is to place the entry at the end of the row

                //Check which bit to change
                if(strcmp(buf[1], global_reg[0]) == 0)
                    config_array[card][adj_global] |= B00001000;   //Set the bit by OR'ing that specific bit as a 1
                else if(strcmp(buf[1], global_reg[1]) == 0)         //with the original byte.
                    config_array[card][adj_global] |= B00000100;
                else if(strcmp(buf[1], global_reg[2]) == 0)
                    config_array[card][adj_global] |= B00000010;
                else if(strcmp(buf[1], global_reg[3]) == 0)
                    config_array[card][adj_global] |= B00000001;
                else
                {
                    //Catch any incorrect bit labels
                    Serial.println();
                    Serial.print("Bit Not Recognized");
                }
            }
        }
        //Error message
        else
            Serial.print("Invalid Channel Range");
    }

    else
    {
        //Check the range of the channels given
        if((start_index > -1) && (end_index < 480))
        {
            //If range was valid, edit all of the given channels
            for(raw_channel = start_index; raw_channel <= end_index; raw_channel++)
            {
                //Map the channel number to an entry in the array
                card = ceil(raw_channel / num_channel);
                adj_channel = raw_channel % num_channel;

                //Check which bit to change
                if(strcmp(buf[1], channel_reg[0]) == 0)
                    config_array[card][adj_channel] |= B10000000;   //Set the bit by OR'ing that specific bit as a 1
                else if(strcmp(buf[1], channel_reg[1]) == 0)        //with the original byte.
                    config_array[card][adj_channel] |= B01000000;
                else if(strcmp(buf[1], channel_reg[2]) == 0)
                    config_array[card][adj_channel] |= B00100000;
                else if(strcmp(buf[1], channel_reg[3]) == 0)
                    config_array[card][adj_channel] |= B00010000;
                else if(strcmp(buf[1], channel_reg[4]) == 0)
                    config_array[card][adj_channel] |= B00001000;
                else if(strcmp(buf[1], channel_reg[5]) == 0)
                    config_array[card][adj_channel] |= B00000100;
                else if(strcmp(buf[1], channel_reg[6]) == 0)
                    config_array[card][adj_channel] |= B00000010;
                else if(strcmp(buf[1], channel_reg[7]) == 0)
                    config_array[card][adj_channel] |= B00000001;
                else
                {
                    //Catch any incorrect bit labels
                    Serial.println();
                    Serial.print("Bit Not Recognized");
                }
            }
        }
        //Error message
        else
            Serial.print("Invalid Channel Range");
    }
}

void clear_bit(char* buf[])
{
    //Local Variable Declarations
    int start_index, end_index, raw_channel, raw_global;
    byte card, adj_channel, adj_global;

    //Get the start_index and end_index numbers
    start_index = strtol(buf[2], NULL, 10) - 1;

    //Work-around to not have a seperate clear_global bit command, and still
    //maintain the optional end argument
    if(strcmp(buf[3], "global") == 0)
    {
        buf[3] = NULL;
        buf[4] = "global";
    }

    //If an end_index number was entered
    if(buf[3] != NULL)
        //Store the number
        end_index = strtol(buf[3], NULL, 10) - 1;
    else
        //If no end_index was given, then set the end_index equal to the start_index.
        //This then only edits a single byte.
        end_index = start_index;

    //Check if global or channel bits
    if(strcmp(buf[4], "global") == 0)
    {
        if((start_index > -1) && (end_index < 30))
        {
            //If range was valid, edit all of the given channels
            for(raw_global = start_index; raw_global <= end_index; raw_global++)
            {
                //Map the channel number to an entry in the array
                card = ceil(raw_global / num_global);
                adj_global = raw_global % num_global + 48;  //The +48 is to place the entry at the end of the row

                //Check which bit to change
                if(strcmp(buf[1], global_reg[0]) == 0)
                    config_array[card][adj_global] &= B11110111;   //Clear the bit by AND'ing that specific bit as a 0
                else if(strcmp(buf[1], global_reg[1]) == 0)         //with the original byte.
                    config_array[card][adj_global] &= B11111011;
                else if(strcmp(buf[1], global_reg[2]) == 0)
                    config_array[card][adj_global] &= B11111101;
                else if(strcmp(buf[1], global_reg[3]) == 0)
                    config_array[card][adj_global] &= B11111110;
                else
                {
                    //Catch any incorrect bit labels
                    Serial.println();
                    Serial.print("Bit Not Recognized");
                }
            }
        }
        //Error message
        else
            Serial.print("Invalid Channel Range");
    }

    else
    {
        //Check the range of the channels given
        if((start_index > -1) && (end_index < 480))
        {
            //If range was valid, edit all of the given channels
            for(raw_channel = start_index; raw_channel <= end_index; raw_channel++)
            {
                //Map the channel number to an entry in the array
                card = ceil(raw_channel / num_channel);
                adj_channel = raw_channel % num_channel;

                //Check which bit to change
                if(strcmp(buf[1], channel_reg[0]) == 0)
                    config_array[card][adj_channel] &= B01111111;   //Clear the bit by AND'ing that specific bit as a 0
                else if(strcmp(buf[1], channel_reg[1]) == 0)        //with the original byte.
                    config_array[card][adj_channel] &= B10111111;
                else if(strcmp(buf[1], channel_reg[2]) == 0)
                    config_array[card][adj_channel] &= B11011111;
                else if(strcmp(buf[1], channel_reg[3]) == 0)
                    config_array[card][adj_channel] &= B11101111;
                else if(strcmp(buf[1], channel_reg[4]) == 0)
                    config_array[card][adj_channel] &= B11110111;
                else if(strcmp(buf[1], channel_reg[5]) == 0)
                    config_array[card][adj_channel] &= B11111011;
                else if(strcmp(buf[1], channel_reg[6]) == 0)
                    config_array[card][adj_channel] &= B11111101;
                else if(strcmp(buf[1], channel_reg[7]) == 0)
                    config_array[card][adj_channel] &= B11111110;
                else
                {
                    //Catch any incorrect bit labels
                    Serial.println();
                    Serial.print("Bit Not Recognized");
                }
            }
        }
        //Error message
        else
            Serial.print("Invalid Channel Range");
    }
}

//Send a test pulse to the asic
void test_signal(char* buf[])
{
    //Declare variables
    int card_index;

    if(buf[1] != NULL)
    {
        //Get the card_index number
        card_index = strtol(buf[1], NULL, 10) - 1;

        //Loop over all 48 channels on one card
        for(int i = 0; i < 48; i++)
        {
            //Enable test capacitance
            config_array[card_index][i] = 0x80;

            //Shift out array / program asic
            buf[1] = "all";
            buf[2] = NULL;
            buf[3] = NULL;
            shift_array(buf);

            //Create two test pulses
            for(int j = 0; j < 2; j++)
            {
                //Square wave of some frequency
                for(int k = 0; k < 1000; k++)
                {
                    digitalWrite(LED_pin, LOW);
                    digitalWrite(LED_pin, HIGH);
                }

                //Pause between pulses
                digitalWrite(LED_pin, LOW);
                delay(500);
            }

            //Turn off test capacitance
            config_array[card_index][i] = 0x00;
        }
    }

    else
    {
        for(int j = 0; j < 2; j++)
        {
            //Square wave of some frequency
            for(int k = 0; k < 1000; k++)
            {
                digitalWrite(LED_pin, LOW);
                digitalWrite(LED_pin, HIGH);
            }

            //Pause between pulses
            digitalWrite(LED_pin, LOW);
            delay(500);
        }
    }
}

//Display Help Message
void help(const char* help_table[])
{
    //Buffer for string
    char help_buffer[128];

    Serial.println();
    for(int i = 0; i < 10; i++)
    {
        //Copy the corresponding line from program memory into the buffer
        strcpy_P(help_buffer, (char*)pgm_read_word(&(help_table[i])));
        Serial.println(help_buffer);
    }
}
