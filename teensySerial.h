//Program: C++ Class Wrapper for Teensy Command Line
//Author : Andrew Cudd
//Email  : cuddandr@msu.edu
//Date   : 6/27/14
//Version: 1.00
////////////////////////////////////////////////////

#ifndef TEENSYSERIAL_H
#define TEENSYSERIAL_H

//Includes
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <fstream>
#include <iostream>
using namespace std;

//Class for talking to a serial port and the attached teensy
class teensySerial
{
    private:
        int debug;      //Debug output flag
        int output;     //Normal output flag
        int fd;         //File Descriptor
        
        struct termios tty;         //Structure for serial port options
        struct termios stdio;       //Structure for standard out options
        struct termios stdio_old;   //Structure for orignal stdio options
        
        int sendCmd(const char* cmd);   //Send a command to the teensy
        void readSerial();              //Read data on the serial port
        
    public:
        //Default constructors
        teensySerial() {debug = 0; output = 0; fd = -1;}
        teensySerial(int d, int o) {debug = d; output = o;}
    
        //Serial port setup
        int initSerial(const char* serialPort);
        void closeSerial();
        //Brings up a terminal-like setup to interact with the teensy
        void debugConsole();
        
        //Functions to send specific commands and arguments to the teensy
        int print(const char* base = "16");
        int edit(const char* val, const char* start, const char* end = NULL);
        int global(const char* val, const char* start, const char* end = NULL);
        int reset();
        int save();
        int load();
        int setBit(const char* bit, const char* start, const char* end = NULL, const char* type = NULL);
        int clearBit(const char* bit, const char* start, const char* end = NULL, const char* type = NULL);
        int shift(const char* delay);
        int help();
        
        //Load and send an entire configuration array from a file
        void sendArray(const char* filename);
        
        //Inline accessor functions
        void setDebug(int d) {debug = d;}
        void setOutput(int o) {output = o;}
        void setFile(int f) {fd = f;}
};

#endif
