//Program: C++ Class Wrapper for Teensy Command Line
//Author : Andrew Cudd
//Email  : cuddandr@msu.edu
//Date   : 6/27/14
//Version: 1.00
////////////////////////////////////////////////////

#include "teensySerial.h"

//Initialize Serial Port
int teensySerial::initSerial(const char* serialPort)
{
    //Open serial port in read/write, non-blocking mode
    fd = open(serialPort, O_RDWR | O_NONBLOCK );
    
    //Error checking
    if (fd == -1)
    {
        cout << "teensySerial_init: Unable to open port. \n";
        return -1;
    }

    if (tcgetattr(STDOUT_FILENO, &stdio_old) < 0)
    {
        cout << "teensySerial_init: Couldn't get stdio attributes. \n";
        return -1;
    }
        
    if (tcgetattr(fd, &tty) < 0)
    {
        cout << "teensySerial_init: Couldn't get serial port attributes. \n";
        return -1;
    }

    //Set baud rate
    cfsetispeed(&tty, B19200); //9600, 19200, 38400
    cfsetospeed(&tty, B19200); //9600, 19200, 38400

    //Set standard out parameters
    memset(&stdio, 0, sizeof(stdio));
    stdio.c_iflag = 0;
    stdio.c_oflag = 0;
    stdio.c_cflag = 0;
    stdio.c_lflag = 0;
    stdio.c_cc[VMIN] = 0;
    stdio.c_cc[VTIME] = 0;
    tcsetattr(STDOUT_FILENO, TCSANOW, &stdio);
    tcsetattr(STDOUT_FILENO, TCSAFLUSH, &stdio);
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);       
    
    //Set serial port parameters
    memset(&tty, 0, sizeof(tty));
    tty.c_iflag = 0;
    tty.c_oflag = 0;
    tty.c_cflag = CS8|CREAD|CLOCAL;           
    tty.c_lflag = 0;
    tty.c_cc[VMIN] = 0;   
    tty.c_cc[VTIME] = 0;  
    
    /*
    tty.c_cflag &= ~PARENB;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;
    tty.c_cflag &= ~CRTSCTS;
    tty.c_cflag |= CREAD | CLOCAL;
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    tty.c_oflag &= ~OPOST;
    tty.c_cc[VMIN]  = 0;
    tty.c_cc[VTIME] = 0;
    */
    
    //Apply serial port parameters
    tcsetattr(fd, TCSANOW, &tty);  
    return fd;
}

//Close serial port
void teensySerial::closeSerial()
{
    //Restore standard out settings
    tcsetattr(STDIN_FILENO, TCSANOW, &stdio_old);
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &stdio_old);
    
    //Flush all data in the buffer
    if(tcflush(fd, TCIOFLUSH) == -1)
        cout << "ERROR\n";
    
    //Close serial port
    close(fd);
}

//Allows for termial-like interface with the teensy
void teensySerial::debugConsole()
{
    //Variable for data
    char c;
    
    //Loop for input/output on the serial port / standard out
    while (c != 'q')
    {
        if (read(fd, &c, 1) > 0)        
            write(STDOUT_FILENO, &c, 1);  
    
        if (read(STDIN_FILENO, &c, 1) > 0)  
            write(fd, &c, 1);                    
    }
}

//Read data on the Serial Port
void teensySerial::readSerial()
{
    //Variable for data
    char c;
    
    //Debug output
    if(debug > 0)
        printf("Reading Serial...\n");
    
    //This is a little odd, but it forces the reading of data since there is
    //a small delay in getting the data from the teensy; it also makes sure
    //that the routine can not get hung up waiting for non-existant data
    for(unsigned int i = 0; i < 16384; i++)
    {
        if(read(fd, &c, 1) > 0)
            write(STDOUT_FILENO, &c, 1);
    }
    //Flush buffers
    if(tcflush(fd, TCIOFLUSH) == -1)
        cout << "ERROR\n";
}

//Send a command to the teensy
int teensySerial::sendCmd(const char* cmd)
{
    //Debug output
    if(debug > 0)
        cout << "Send Command: " << cmd << endl;

    //Set up and write command
    int length = strlen(cmd);
    int error = write(fd, cmd, length);
    
    //Error check
    if(error != length)
    {
        printf("teensySerial_sendCmd: couldn't write whole command. error: %d\n", error);
        return -1;
    }

    //Output data if flag is set
    if(output > 0)
        readSerial();
    else
        usleep(786);
    //If not reading output, a small delay is needed to allow for the teensy
    //to handle the flow of commands.
    
    //Return a-ok        
    return 0;    
}

//Print command
int teensySerial::print(const char* base)
{
    //Variables for the command
    char command[16] = "";
    const char* cmd = "print ";
    
    //Construct command
    strcat(command, cmd);
    strcat(command, base);
    strcat(command, "\r");
    
    //Debug output
    if(debug > 0)
        cout << "Print Command: " << command << endl;
    
    //Send command and return error flag
    return sendCmd(command);
}

//Channel Register Edit command
int teensySerial::edit(const char* val, const char* start, const char* end)
{
    char command[16] = "";
    const char* cmd = "edit ";
    
    strcat(command, cmd);
    strcat(command, val);
    strcat(command, " ");
    strcat(command, start);
    if(end != NULL)
    {
        strcat(command, " ");
        strcat(command, end);
    }
    strcat(command, "\r");

    if(debug > 0)
        cout << "Edit Command: " << command << endl;

    return sendCmd(command);
}

//Global Register Edit command
int teensySerial::global(const char* val, const char* start, const char* end)
{
    char command[16] = "";
    const char* cmd = "global ";
    
    strcat(command, cmd);
    strcat(command, val);
    strcat(command, " ");
    strcat(command, start);
    if(end != NULL)
    {
        strcat(command, " ");
        strcat(command, end);
    }
    strcat(command, "\r");

    if(debug > 0)
        cout << "Global Command: " << command << endl;

    return sendCmd(command);
}

//Reset configuration array
int teensySerial::reset()
{
    const char* cmd = "reset\r";

    if(debug > 0)
        cout << "Reset Command: " << cmd << endl;
    
    return sendCmd(cmd);
}

//Save configuration array to EEPROM
int teensySerial::save()
{
    const char* cmd = "save\r";
     
    if(debug > 0)
        cout << "Save Command: " << cmd << endl;
    
    return sendCmd(cmd);
}

//Load configuration array from EEPROM
int teensySerial::load()
{
    const char* cmd = "load\r";
    
    if(debug > 0)
        cout << "Load Command: " << cmd << endl;
    
    return sendCmd(cmd);
}

//Set a bit in any register
int teensySerial::setBit(const char* bit, const char* start, const char* end, const char* type)
{
    char command[24] = "";
    const char* cmd = "set ";
    
    strcat(command, cmd);
    strcat(command, bit);
    strcat(command, " ");
    strcat(command, start);
    if(end != NULL)
    {
        strcat(command, " ");
        strcat(command, end);
    }
    if(type != NULL)
    {
        strcat(command, " ");
        strcat(command, type);
    }
    strcat(command, "\r");
    
    if(debug > 0)
        cout << "Set Bit Command: " << command << endl;

    return sendCmd(command);
}

//Clear a bit in any register
int teensySerial::clearBit(const char* bit, const char* start, const char* end, const char* type)
{
    char command[24] = "";
    const char* cmd = "clear ";
    
    strcat(command, cmd);
    strcat(command, bit);
    strcat(command, " ");
    strcat(command, start);
    if(end != NULL)
    {
        strcat(command, " ");
        strcat(command, end);
    }
    if(type != NULL)
    {
        strcat(command, " ");
        strcat(command, type);
    }
    strcat(command, "\r");
    
    if(debug > 0)
        cout << "Clear Bit Command: " << command << endl;

    return sendCmd(command);    
}

//Shift data from teensy to LArASIC's
int teensySerial::shift(const char* delay)
{
    char command[16] = "";
    const char* cmd = "shift ";
    
    strcat(command, cmd);
    strcat(command, "all ");
    strcat(command, "rising ");
    strcat(command, delay);
    strcat(command, "\r");
    
    if(debug > 0)
        cout << "Shift Command: " << command << endl;
    
    return sendCmd(command);
}

//Display Help
int teensySerial::help()
{
    const char* cmd = "help\r";
    
    if(debug > 0)
        cout << "Help Command: " << cmd << endl;
    
    return sendCmd(cmd);    
}

//Load and send an entire configuration array from a file
void teensySerial::sendArray(const char* filename)
{
    //Index variables
    int i = 0;
    int l = 0;
    
    //Create arrays for the data
    char** array;
    array = new char* [512];
    for(int j = 0; j < 512; j++)
        array[j] = new char[3];
    
    char** index;
    index = new char* [512];
    for(int k = 0; k < 512; k++)
        index[k] = new char[3];
        
    //Open file
    ifstream config;
    config.open(filename, ios::in);
    
    //If successful...
    if(config.is_open())
    {
        //Read until end-of-file is reached
        while(!config.eof())
        {
            //Store data and index number as char arrays
            config >> array[i++];
            sprintf(index[l++], "%d", l+1);
        }
        //Close file
        config.close();
        
        //Send each data point one by one
        for(int m = 0; m < 480; m++)
        {
            edit(array[m], index[m]);
            usleep(768);
            //Necessary delay to allow the teensy to handle all the writes
        }
            
        for(int n = 0; n < 30; n++)
        {
            global(array[n+480], index[n]);
            usleep(768);
        }
    }
    //Error output
    else
        cout << "Could not open file!" << endl;
}
